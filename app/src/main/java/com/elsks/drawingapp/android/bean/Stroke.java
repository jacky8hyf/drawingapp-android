package com.elsks.drawingapp.android.bean;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.graphics.Path;

import com.avos.avoscloud.AVObject;
import com.elsks.drawingapp.android.Constants;

public class Stroke {

//    private static final long serialVersionUID = -4578115445783471333L;
    
    public static final String CLASS_NAME = "Stroke";
    
    private final Object mutex = new Object();
    private volatile boolean finished = false;
    private final Path path = new Path();
    
    // ~--- points related
    private final ArrayList<float[]> points = new ArrayList<float[]>();
        
    public Stroke(float x, float y) {
//        super(CLASS_NAME);
        path.moveTo(x, y);
        points.add(new float[]{x, y});
    }

    public static Stroke from(AVObject avObject)
        throws IllegalArgumentException {
        if(avObject == null)
            return null;
        List<?> lst = avObject.getList(Constants.STROKE_DATA);
        return from(lst);
    }
    
    public static Stroke from(List<?> points) {
        if(points == null || points.isEmpty())
            return null;
        Stroke s = null;
        for(Object pair : points) {
//            float[] f = new float[2];
            float x, y;
            if(pair.getClass().isArray()) {
                if(Array.getLength(pair) != 2)
                    throw new IllegalArgumentException("wrong array length of " + pair 
                            + ":" + Array.getLength(pair) + " is not 2");
                x = floatValue(Array.get(pair, 0));
                y = floatValue(Array.get(pair, 1));
            } else if (pair instanceof List) {
                List<?> pairList = (List<?>)pair;
                if(pairList.size() != 2)
                    throw new IllegalArgumentException("wrong array length of " + pair 
                            + ":" + Array.getLength(pair) + " is not 2");
                x = floatValue(pairList.get(0));
                y = floatValue(pairList.get(1));
            } else 
                throw new IllegalArgumentException("cannot parse " + pair);
            
            if(s == null)
                s = new Stroke(x,y);
            else 
                s.addPoint(x,y);
        }
        return s;
    }
    
    public AVObject toAVObject(String canvasId) {
        AVObject obj = new AVObject(CLASS_NAME);
        obj.put(Constants.STROKE_DATA, getPoints());
        obj.put(Constants.STROKE_CANVAS_ID, canvasId);
        return obj;
    }
    
    private static float floatValue(Object o) {
        if(o instanceof Number) 
            return ((Number) o).floatValue();
        if(o instanceof CharSequence)
            return Float.parseFloat(o.toString());
        throw new IllegalArgumentException(o + " is not a number");
    }
    

    // http://stackoverflow.com/a/7058606/4435092
    // for a curve through all points, try http://cartogrammar.com/source/CubicBezier.as
    public void addPoint(float x, float y) {
        synchronized (mutex) {
            if(finished)
                throw new IllegalStateException("finished");
            float[] last = points.get(points.size() - 1);
            float lastX = last[0], lastY = last[1];
            if(Math.abs(lastX - x) + Math.abs(lastY - y) < Constants.DISTANCE_THRESHOLD) 
                return;
            float xc = (lastX + x) / 2, yc = (lastY + y) / 2;
            path.quadTo(lastX, lastY, xc, yc);
            points.add(new float[]{x, y});
        }
    }
    
    public void finish() {
        synchronized (mutex) {
            finished = true;
            int size = points.size();
            if(size < 2) 
                return; // only 1 point, don't do anything.
            float[] secondLast = points.get(size - 2), last = points.get(size - 1);
            // finish drawing path
            path.quadTo(secondLast[0], secondLast[1], last[0], last[1]);
//            super.put(Constants.STROKE_DATA, points);
        }
    }
    
    /**
     * 
     * @return internal path object. never null.
     */
    public Path getPath() {
        return path;
    }

    public boolean finished() {
        return finished;
    }
    
    /**
     * 
     * @return An unmodifiable view of the points list.
     */
    public List<float[]> getPoints() {
        return Collections.unmodifiableList(points);
    }
    
}
