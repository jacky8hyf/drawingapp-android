package com.elsks.drawingapp.android;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

//http://www.bkjia.com/Androidjc/729017.html
public class TutorialActivity extends Activity implements ViewPager.OnPageChangeListener {
    
    private static final int NUM_PAGES = 4;
    
    private ViewPager pager;
    private ImageView[] mPageIndicator=new ImageView[NUM_PAGES];
//    public static final String NEXT_ACTIVITY_KEY = "nextActivity";
//    private String nextActivity;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        nextActivity = getIntent().getStringExtra(NEXT_ACTIVITY_KEY);
        setContentView(R.layout.activity_tutorial);
        pager=(ViewPager)findViewById(R.id.viewPager);
        pager.addOnPageChangeListener(this);

        LinearLayout indicators = (LinearLayout)findViewById(R.id.page_indicators);
        final View[] views = new View[NUM_PAGES + 1];


        int indicatorMargin = Math.round(getResources().getDimension(R.dimen.pager_indicator_gap));

        for(int i=0;i<NUM_PAGES;i++){
            try {
                views[i] = getLayoutInflater().inflate(
                        R.layout.class.getField("tutorial"+i).getInt(null), null);
            } catch (Exception e) {
                if(e instanceof RuntimeException) throw (RuntimeException)e;
                throw new RuntimeException(e);
            }
            // crate indicator programatically
            ImageView indicator = new ImageView(TutorialActivity.this);
            indicator.setImageResource(R.drawable.dot);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(indicatorMargin,0,0,0);
            lp.gravity = Gravity.CENTER_VERTICAL;
            indicator.setLayoutParams(lp);
            indicators.addView(indicator);
            mPageIndicator[i]=indicator;

        }
        mPageIndicator[0].setImageResource(R.drawable.dot_selected);

        views[NUM_PAGES] = new View(this); // fake view 
        
        PagerAdapter adapter=new PagerAdapter(){
            @Override
            public boolean isViewFromObject(View arg0,Object arg1){
                return arg0==arg1;
            }
            
            @Override
            public int getCount(){
                return views.length;
            }

            @Override
            public void destroyItem(ViewGroup container,int position,Object object){
                container.removeView(views[position]);
            }
            
            @Override
            public Object instantiateItem(ViewGroup container,int position){
                container.addView(views[position]);
                return views[position];
            }
        };
        pager.setAdapter(adapter);
        
        
    }

    @Override
    public void onPageSelected(int page){
        if(page >= 0 && page < mPageIndicator.length) {
            mPageIndicator[page].setImageDrawable(getResources().
                    getDrawable(R.drawable.dot_selected));
            if(page != 0) { // is not first true page
                mPageIndicator[page-1].setImageDrawable(getResources().
                        getDrawable(R.drawable.dot));
            }
            if(page != mPageIndicator.length - 1){ // is not last true page
                mPageIndicator[page+1].setImageDrawable(getResources().
                        getDrawable(R.drawable.dot));
            }
        }
        if(page == NUM_PAGES) { // is on fake page
            finish();
//                if(!Utils.isEmpty(nextActivity)) {
//                    try {
//                        Class<?> activityClass = Class.forName(nextActivity);
//                        // http://stackoverflow.com/a/16359269/4435092
//                        startActivity(new Intent(
//                                TutorialActivity.this, activityClass)
//                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                    } catch (ClassNotFoundException e) { }
//                }
        }
    }

    @Override
    public void onPageScrolled(int arg0,float arg1,
                               int arg2){
    }
    @Override
    public void onPageScrollStateChanged(int arg0){
    }
    
}