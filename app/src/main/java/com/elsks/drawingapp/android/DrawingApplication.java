package com.elsks.drawingapp.android;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.SaveCallback;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public class DrawingApplication extends Application {

    private static DrawingApplication instance;
    /**
     * A handler which handles messages that what is one of
     * the constants defined in {@link Constants.MessageId}
     */
    private Handler handler;
    private static volatile WeakReference<BaseActivity> currentActivity
        = new WeakReference<BaseActivity>(null);
    
    private static final WeakHashMap<Handler.Callback, Void> messageHandlers 
        = new WeakHashMap<Handler.Callback, Void>();

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                for(Handler.Callback cb : messageHandlers.keySet()) {
                    cb.handleMessage(msg);
                    // does not stop if returning true, because messages is broadcasted.
                }
            }
        };
        AVOSCloud.initialize(this, Constants.APP_ID, Constants.APP_KEY);
        
        
    }
    
    public static Handler getHandler() {
        return instance.handler;
    }
    
    public static void postCallback(final SaveCallback callback, final AVException arg) {
        getHandler().post(new Runnable() {
           public void run() {
               callback.done(arg);
           }
        });
    }
    
    public static void showToast(final int resId) {
        getHandler().post(new Runnable() {
            @Override
            public void run() {
                Activity a = currentActivity.get();
                if(a != null)
                    Toast.makeText(a, resId, Toast.LENGTH_LONG).show();
            }
        });
    }
    public static void showToast(final String str) {
        getHandler().post(new Runnable() {
            @Override
            public void run() {
                Activity a = currentActivity.get();
                if(a != null)
                    Toast.makeText(a, str, Toast.LENGTH_LONG).show();
            }
        });
    }
    
    public static void register(BaseActivity a) {
        messageHandlers.put(a, null);
    }
    public static void deregister(BaseActivity a) {
        messageHandlers.remove(a);
    }
    
    public static void sendMessage(int what, int arg1, int arg2, Bundle b) {
        Log.d("DrawingApplication", "sending message " + what);
        Message msg = Message.obtain(instance.handler, what, arg1, arg2);
        if(b != null)msg.setData(b);
        instance.handler.sendMessage(msg);
    }
    
    public static void sendMessage(int what, int arg1, int arg2, Serializable... kvPairs) {
        Log.d("DrawingApplication", "sending message " + what);
        Message msg = Message.obtain(instance.handler, what, arg1, arg2);
        Bundle b = new Bundle();
        for(int i = 1, len = kvPairs.length; i < len; i+=2) {
            b.putSerializable((String)kvPairs[i - 1], kvPairs[i]);
        }
        msg.setData(b);
        instance.handler.sendMessage(msg);
    }
    
    static void setCurrentActivity(BaseActivity c) {
        currentActivity = new WeakReference<BaseActivity>(c);
    }
    
    public static BaseActivity getCurrentActivity() {
        return currentActivity.get();
    }

    public static DrawingApplication getInstance() {
        return instance;
    }
}


