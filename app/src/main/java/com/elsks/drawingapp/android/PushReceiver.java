package com.elsks.drawingapp.android;

import java.io.IOException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.elsks.drawingapp.android.bean.StrokeBean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PushReceiver extends BroadcastReceiver {

    public static final ObjectMapper mapper = new ObjectMapper();
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if("com.elsks.drawingapp.android.STROKE_ADDED".equals(action)) {
            try {
                StrokeBean bean = mapper
                    .treeToValue(
                        mapper.readValue(
                            intent.getExtras().getString("com.avos.avoscloud.Data"),
                            JsonNode.class).get("stroke"),
                        StrokeBean.class);
                Log.d("PushReceiver", "canvasId = " + bean.getCanvasId()
                                + ", strokes = " + bean.getStrokeId()
                                + ", points = " + bean.getPoints());
                CanvasService.instance.addStrokeFromServer(bean);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
