package com.elsks.drawingapp.android;

public class Utils {
    public static final boolean isEmpty(CharSequence e) {
        return e == null || e.length() == 0;
    }
}
