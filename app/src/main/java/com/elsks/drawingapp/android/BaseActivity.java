package com.elsks.drawingapp.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public abstract class BaseActivity extends Activity implements Handler.Callback {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DrawingApplication.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DrawingApplication.deregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DrawingApplication.setCurrentActivity(this);
    }


    public boolean handleMessage(Message msg) {
        return false;
    }
}
