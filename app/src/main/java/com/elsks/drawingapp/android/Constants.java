package com.elsks.drawingapp.android;

public final class Constants {
    public static final boolean DRAW_POINTS = false /* BuildConfig.DEBUG */;
    public static final String APP_ID = "dnpcv05kbk60qwdb9ye4iue1egkoxbb1pl5enhigcmgn5mjr";
    public static final String APP_KEY = "clzyujd6z72dg6er5c3p1iizbj6sc4hz0v00zrkqg6t9bz52";
    public static final String STROKE_DATA          = "points";
    public static final String STROKE_CANVAS_ID     = "canvasId";
    public static final String CANVAS_STROKES = "strokes";
    public static final float DISTANCE_THRESHOLD = 2f;
    
    public final class MessageId {
        public static final int 
            CANVAS_CHANGE = 1,
            RECEIVED_STROKE_FROM_SERVER = 2,
            TOGGLED_FAVORITE = 3;
        
    }
}
