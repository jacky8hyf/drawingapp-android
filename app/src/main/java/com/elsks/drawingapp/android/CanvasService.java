package com.elsks.drawingapp.android;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import android.os.AsyncTask;
import android.util.Log;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.PushService;
import com.avos.avoscloud.SaveCallback;
import com.elsks.drawingapp.android.bean.Stroke;
import com.elsks.drawingapp.android.bean.StrokeBean;

public enum CanvasService {
    instance;
    
//    private CanvasBean canvasData;
    
    private final Object mutex = new Object();
    
    private String canvasId, loadingCanvasId;
    /**
     * A list of stroke ids that are confirmed to be stored on server
     */
    private final Set<String> strokesOnServer = new HashSet<String>();
    
    
    /**
     * Strokes to display on the screen.
     */
    private final List<Stroke> allStrokesOnScreen = new ArrayList<Stroke>();
    
    /**
     * Strokes received from server, but yet to be displayed;
     */
    private final Queue<Stroke> strokesToDraw = new LinkedList<Stroke>();
    
    private final Random r = new Random();
    
    public boolean isCanvasSpecified() {

//        return canvasData != null && !Utils.isEmpty(canvasData.getObjectId());
        synchronized (mutex) {
            return !Utils.isEmpty(canvasId);
        }
    }
    
    // ~--- the only three way to modify canvasData

    
    public void openRandomCanvas() {
        openCanvas(Integer.toHexString(r.nextInt()));
    }
    
    public void openCanvas(final String canvasId) {
        if(Utils.isEmpty(canvasId))
            throw new IllegalArgumentException("empty canvas id");
        synchronized (mutex) {
            if(canvasId.equals(loadingCanvasId))
                return; // abort; already loading.
        }
        
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                DrawingApplication.showToast(
                    DrawingApplication.getInstance().getString(R.string.loading_canvas) + " " + canvasId);
            }
            @Override
            protected Void doInBackground(Void... params) {
                synchronized (mutex) {
                    loadingCanvasId = canvasId;
                }
                List<AVObject> objects, fromServer;
                try {
                    objects= new ArrayList<AVObject>();
                    do {
                        fromServer = new AVQuery<AVObject>(Stroke.CLASS_NAME)
                            .whereEqualTo(Constants.STROKE_CANVAS_ID, canvasId)
                            .orderByAscending("createdAt") // so the list will be consistent
                            .setSkip(objects.size())
                            .find();
                        synchronized (mutex) {
                            if(!canvasId.equals(loadingCanvasId)) 
                                return null; // abort; loading another canvas
                        }
                        objects.addAll(fromServer);
//                        for(AVObject obj : fromServer) {
//                            Log.d("CanvasService", "loaded stroke " + obj.getObjectId() + " created at " + obj.getCreatedAt());
//                        }
                    } while(!fromServer.isEmpty());
                    
                    // TODO Those strokes created from now to subscription of the new channel are just missing from
                    // the graph. whatever... not a serious bug. Fix later.
                    if(!Utils.isEmpty(CanvasService.this.canvasId))
                        PushService.unsubscribe(DrawingApplication.getInstance(), CanvasService.this.canvasId);
                    PushService.subscribe(DrawingApplication.getInstance(), canvasId, MainActivity.class);
                    AVInstallation.getCurrentInstallation().save();
                } catch (AVException e) {
                    DrawingApplication.showToast(e.getLocalizedMessage());
                    return null; // abort: exception occur
                }
                
                // set appropriate data
                synchronized (mutex) {
                    if(!canvasId.equals(loadingCanvasId)) 
                        return null;// abort; another canvas is loading

                    CanvasService.this.canvasId = canvasId;
                    strokesOnServer.clear();
                    allStrokesOnScreen.clear();
                    for(AVObject obj : objects) {
                        strokesOnServer.add(obj.getObjectId());
                        Stroke b = Stroke.from(obj);
                        if(b != null) allStrokesOnScreen.add(b);
                    }
                    loadingCanvasId = null; // done loading
                    DrawingApplication.sendMessage(Constants.MessageId.CANVAS_CHANGE, 0, 0);
                }
                return null;
            }
            
        }.execute();        
    }
    
    /**
     * 
     * @param stroke
     * @throws IllegalStateException canvasData not initialized properly
     */
    public void addStroke(final Stroke stroke) throws IllegalStateException, IllegalArgumentException {
        if(!isCanvasSpecified())
            throw new IllegalStateException();
        if(stroke == null)
            throw new IllegalArgumentException();
        if(!stroke.finished())
            throw new IllegalArgumentException("not finished");
        final AVObject obj;
        synchronized (mutex) {
            allStrokesOnScreen.add(stroke);
            obj = stroke.toAVObject(canvasId);
        }
        obj.saveEventually(new SaveCallback() {
            @Override
            public void done(AVException ex) {
                if(ex == null && !Utils.isEmpty(obj.getObjectId())) {
                    Log.d("CanvasService", "saved stroke " + obj.getObjectId());
                    synchronized (mutex) {
                        strokesOnServer.add(obj.getObjectId()); // if exists then ignore it
                    }
                }
            }
        });
//        AVCloud.callFunctionInBackground("saveStroke", new ParameterMap()
//                .putParam("points", stroke.getPoints())
//                .putParam("canvasId", canvasId),
//            new FunctionCallback<HashMap<String, Object>>() {
//                public void done(HashMap<String, Object> object, AVException e) {
//                    Log.i("CanvasService", object == null ? "null" : 
//                        "[" + object.getClass() + "] " + object);
//                    if(callback != null)callback.done(e);
//                }
//            }
//        );
//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... params) {
//                try {
//                    stroke.save();
//                    canvasData.addUnique(Constants.CANVAS_STROKES, stroke);
//                    canvasData.save();
//                    DrawingApplication.postCallback(callback, null);
//                } catch (AVException ex) {
//                    DrawingApplication.postCallback(callback, ex);
//                }
//                return null;
//            }
//        }.execute();
    }
    
    public void addStrokeFromServer(StrokeBean bean) {
        if (bean == null 
                || Utils.isEmpty(bean.getCanvasId())
                || Utils.isEmpty(bean.getStrokeId()))
            throw new IllegalArgumentException();
        synchronized (mutex) {
            if(!bean.getCanvasId().equals(this.canvasId))
                return; // this notification is kinda late! I have changed canvas
            if(strokesOnServer.contains(bean.getStrokeId()))
                return;
            strokesToDraw.add(Stroke.from(bean.getPoints()));
            strokesOnServer.add(bean.getStrokeId());
        }
        DrawingApplication.sendMessage(Constants.MessageId.RECEIVED_STROKE_FROM_SERVER, 0, 0);
    }
    
    public Stroke nextStrokeToDraw() {
        synchronized (mutex) {
            Stroke next = strokesToDraw.poll();
            if(next != null)
                allStrokesOnScreen.add(next);
            return next;
        }
    }
    
    @SuppressWarnings("serial")
    public static class ParameterMap extends HashMap<String, Object> {
        public ParameterMap() { super(); }
        public ParameterMap(Map<? extends String, ? extends Object> m) { super(m); }

        public ParameterMap putParam(String key, Object value) {
            put(key, value);
            return this;
        }
    }

    public String getCanvasId() {
//        return canvasData == null ? null : canvasData.getObjectId();
        synchronized (mutex) {
            return canvasId;
        }
    }
    
    public List<Stroke> getAllStrokesOnScreen() {
        return Collections.unmodifiableList(allStrokesOnScreen);
    }
    
    
    

}
