package com.elsks.drawingapp.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public enum SharePreferencesService {
    instance;
    
    private static final String LAST_APP_VERSION = "lastAppVersion";
    
    private static final String KEY ="favoriteCanvasIds";
    
    private TreeSet<String> s = null;
    
    private SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(
                DrawingApplication.getInstance());
    }
    
    private SharedPreferences init() {
        if(s == null) {
            SharedPreferences sp = getSharedPreferences();
            s =  new TreeSet<String>(sp.getStringSet(KEY, Collections.<String>emptySet()));
            return sp;
        }
        return null;
    }
    
    public boolean toggleFavorite(String canvasId) {
        if(Utils.isEmpty(canvasId))
            throw new IllegalArgumentException("empty canvas id " + canvasId);
        SharedPreferences sp = init();
        if(sp == null) sp = getSharedPreferences();
        boolean prev = s.contains(canvasId);
        if(prev)
            s.remove(canvasId);
        else 
            s.add(canvasId);
        sp.edit().putStringSet(KEY, s).commit();
        DrawingApplication.sendMessage(Constants.MessageId.TOGGLED_FAVORITE,0,0);
        return !prev;
    }
    
    public boolean isFavorite(String canvasId) {
        init();
        return s.contains(canvasId);
    }

    public Set<String> getAllFavoriteCanvases() {
        init();
        return Collections.unmodifiableSet(s);
    }
    
    /**
     * Distinguishes different kinds of app starts: <li>
     * <ul>
     * First start ever ({@link #FIRST_TIME})
     * </ul>
     * <ul>
     * First start in this version ({@link #FIRST_TIME_VERSION})
     * </ul>
     * <ul>
     * Normal app start ({@link #NORMAL})
     * </ul>
     * 
     * @author schnatterer
     * 
     */
    public enum AppStart {
        FIRST_TIME, FIRST_TIME_VERSION, NORMAL
    }
    
    /**
     * Finds out started for the first time (ever or in the current version).<br/>
     * <br/>
     * Note: This method is <b>not idempotent</b> only the first call will
     * determine the proper result. Any subsequent calls will only return
     * {@link AppStart#NORMAL} until the app is started again. So you might want
     * to consider caching the result!
     * 
     * @return the type of app start
     */
    public AppStart checkAppStart() {
        Context c = DrawingApplication.getInstance();
        PackageInfo pInfo;
        SharedPreferences sharedPreferences = getSharedPreferences();
        AppStart appStart = AppStart.NORMAL;
        try {
            pInfo = c.getPackageManager().getPackageInfo(c.getPackageName(), 0);
            int lastVersionCode = sharedPreferences
                    .getInt(LAST_APP_VERSION, -1);
            int currentVersionCode = pInfo.versionCode;
            appStart = checkAppStart(currentVersionCode, lastVersionCode);
            // Update version in preferences
            sharedPreferences.edit()
                    .putInt(LAST_APP_VERSION, currentVersionCode).commit();
        } catch (NameNotFoundException e) {
            Log.w("SharedPreferencesService",
                    "Unable to determine current app version from pacakge manager. Defenisvely assuming normal app start.");
        }
        return appStart;
    }

    private AppStart checkAppStart(int currentVersionCode, int lastVersionCode) {
        if (lastVersionCode == -1) {
            return AppStart.FIRST_TIME;
        } else if (lastVersionCode < currentVersionCode) {
            return AppStart.FIRST_TIME_VERSION;
        } else if (lastVersionCode > currentVersionCode) {
            Log.w("SharedPreferencesService", "Current version code (" + currentVersionCode
                    + ") is less then the one recognized on last startup ("
                    + lastVersionCode
                    + "). Defenisvely assuming normal app start.");
            return AppStart.NORMAL;
        } else {
            return AppStart.NORMAL;
        }
    }
}
