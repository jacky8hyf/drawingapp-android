package com.elsks.drawingapp.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.elsks.drawingapp.android.bean.Stroke;

// http://code.tutsplus.com/tutorials/android-sdk-create-a-drawing-app-touch-interaction--mobile-19202
public final class DrawingView extends View {
    
    //current path
    private Stroke currentStroke;
    //paints
    private Paint drawPaint, canvasPaint, pointPaint, endPointPaint;
    //canvas
    private Canvas drawCanvas;
    //canvas bitmap
    private Bitmap canvasBitmap;
    
//    private final ArrayList<StrokeBean> strokesToDraw = new ArrayList<StrokeBean>();
    
//    private volatile boolean initialized = false;
    
    public DrawingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawingView(Context context) {
        super(context);
    }
    
    private void setupDrawing() {
        currentStroke = null;
        drawPaint = new Paint();
        drawPaint.setColor(Color.BLACK);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(4);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        if(Constants.DRAW_POINTS) {
            pointPaint = new Paint(drawPaint);
            pointPaint.setStrokeWidth(10);
            pointPaint.setColor(Color.RED);
            endPointPaint = new Paint(pointPaint);
            endPointPaint.setColor(Color.GREEN);
        }
        canvasPaint = new Paint(Paint.DITHER_FLAG);
    }
    
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setupDrawing();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d("DrawingView", "onSizeChanged");
        reloadAllStrokes();
    }
    
    public void reloadAllStrokes() {
        Log.d("DrawingView", "reloadAllStrokes: size = " + CanvasService.instance.getAllStrokesOnScreen().size());
        canvasBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
//        drawCanvas.drawColor(Color.LTGRAY);
        for(Stroke stroke : CanvasService.instance.getAllStrokesOnScreen()) {
            if(stroke != null) {
//                Log.d("DrawingView", new JSONArray(stroke.getPoints()).toString());
                drawCanvas.drawPath(stroke.getPath(), drawPaint);
            }
        }
        invalidate();
    }
    
    public void drawStrokesFromServer() {
        Stroke s;
        while((s = CanvasService.instance.nextStrokeToDraw()) != null) {
            drawCanvas.drawPath(s.getPath(), drawPaint);
        }
        invalidate();
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
//        Log.d("DrawingView", "initialized = " + CanvasService.instance.isCanvasSpecified());
        if(!CanvasService.instance.isCanvasSpecified())
            return;
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        if(currentStroke != null) 
            canvas.drawPath(currentStroke.getPath(), drawPaint);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        Log.d("DrawingView", "initialized = " + CanvasService.instance.isCanvasSpecified());
        if(!CanvasService.instance.isCanvasSpecified())
            return false;
        float touchX = event.getX();
        float touchY = event.getY();
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            currentStroke = new Stroke(touchX, touchY);
            if(Constants.DRAW_POINTS)
                drawCanvas.drawPoint(touchX, touchY, endPointPaint);
            break;
        case MotionEvent.ACTION_MOVE:
            currentStroke.addPoint(touchX, touchY);
            if(Constants.DRAW_POINTS)
                drawCanvas.drawPoint(touchX, touchY, pointPaint);
            break;
        case MotionEvent.ACTION_UP:
            currentStroke.finish(); // no need to add, because done in action_move.
            drawCanvas.drawPath(currentStroke.getPath(), drawPaint);
            if(Constants.DRAW_POINTS)
                drawCanvas.drawPoint(touchX, touchY, endPointPaint);
            CanvasService.instance.addStroke(currentStroke);
            currentStroke = null;
            break;
        default:
            return false;
        }
        invalidate(); // the whole view
        return true;
    }

}
