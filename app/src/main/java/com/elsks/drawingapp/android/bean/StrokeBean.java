package com.elsks.drawingapp.android.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StrokeBean {
    
    public static final String POINTS_KEY = "points",
            CANVAS_ID_KEY = "canvasId",
            STROKE_ID_KEY = "objectId";
    
    private StrokeBean() {}

    @JsonProperty(POINTS_KEY)
    private ArrayList<float[]> points;

    @JsonProperty(CANVAS_ID_KEY)
    private String canvasId;
    
    @JsonProperty(STROKE_ID_KEY)
    private String strokeId;
    
    public String getStrokeId() {
        return strokeId;
    }

    public String getCanvasId() {
        return canvasId;
    }

    public List<float[]> getPoints() {
        return Collections.unmodifiableList(points);
    }
}
