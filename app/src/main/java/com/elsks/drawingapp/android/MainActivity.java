package com.elsks.drawingapp.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.Collection;

import static com.elsks.drawingapp.android.Constants.MessageId.CANVAS_CHANGE;
import static com.elsks.drawingapp.android.Constants.MessageId.RECEIVED_STROKE_FROM_SERVER;
import static com.elsks.drawingapp.android.Constants.MessageId.TOGGLED_FAVORITE;
import static com.elsks.drawingapp.android.DrawingApplication.showToast;

public class MainActivity extends BaseActivity
        implements View.OnClickListener, FloatingActionMenu.OnMenuToggleListener {

    private DrawingView drawingView;
    private TextView noCanvasText, titleText;
    private View title;

    private FloatingActionMenu floatingActionMenu;
    private FloatingActionButton copyButton;
    private FloatingActionButton toggleFavButton;

    /**
     * Called when the activity is first created.
     * @param savedInstanceState If the activity is being re-initialized after 
     * previously being shut down then this Bundle contains the data it most 
     * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActionBar().hide();
        setContentView(R.layout.activity_main);

        title = findViewById(R.id.title);
        titleText = (TextView)findViewById(R.id.text_view_title);
        drawingView = (DrawingView)findViewById(R.id.drawingView);
        noCanvasText = (TextView)findViewById(R.id.no_canvas_text);
        floatingActionMenu = (FloatingActionMenu)findViewById(R.id.floating_action_menu);

        // listeners
        for(View v : new View[]{
                (copyButton = (FloatingActionButton)findViewById(R.id.btn_copy_id)),
                (toggleFavButton = (FloatingActionButton)findViewById(R.id.btn_toggle_fav)),
                findViewById(R.id.btn_about),
                findViewById(R.id.btn_open_canvas),
                findViewById(R.id.btn_open_fav),
                findViewById(R.id.btn_tutorial)
        }) {
            v.setOnClickListener(this);
        }
        floatingActionMenu.setOnMenuToggleListener(this);

        // replaces text for specific canvas id
        showCanvasId(CanvasService.instance.getCanvasId());

        // if need to, show tutorial for first app start
        switch(SharePreferencesService.instance.checkAppStart()) {
            case FIRST_TIME:
            case FIRST_TIME_VERSION:
                gotoTutorial();
                break;
            default:
        }
    }

    private void showCanvasId(String canvasId) {
        boolean empty = Utils.isEmpty(canvasId);
        drawingView.setVisibility(empty ? View.INVISIBLE : View.VISIBLE);
        noCanvasText.setVisibility(empty ? View.VISIBLE : View.GONE);
        if(empty) {
            String noCanvas = getString(R.string.no_canvas_simple);
            setTitle(noCanvas);
            copyButton.setEnabled(false);
            toggleFavButton.setEnabled(false);
        } else {
            setTitle(canvasId);
            copyButton.setLabelText((getString(R.string.copy) + " " + canvasId));
            copyButton.setEnabled(true);
            boolean isFav = SharePreferencesService.instance.isFavorite(canvasId);
            toggleFavButton.setLabelText(getString(isFav
                    ? R.string.remove_from_fav : R.string.add_to_fav));
            toggleFavButton.setImageResource(isFav
                    ? R.drawable.ic_star_border
                    : R.drawable.ic_star);
            toggleFavButton.setEnabled(true);
        }
    }

    public void setTitle(CharSequence cs) {
        super.setTitle(cs);
        titleText.setText(cs);
    }

    private void copyId() {
        String canvasId = CanvasService.instance.getCanvasId();
        if(!Utils.isEmpty(canvasId)) {
            ClipboardManager.copyToClipboard(this, canvasId);
            showToast(canvasId + " " + getString(R.string.copied));
        } else {
            showToast(R.string.no_canvas_simple);
        }
    }
    private void openCanvas() {
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_canvas_id, null);
        new AlertDialog.Builder(this)
                .setTitle(R.string.action_open_canvas)
                .setView(dialogView)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String id = ((EditText)dialogView.findViewById(R.id.editTextCanvasId)).getText().toString();
                        openCanvasOrShowToast(id);
                    }
                })
                .setNeutralButton(R.string.random_canvas_id, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CanvasService.instance.openRandomCanvas();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create().show();
    }
    private void toggleFav() {
        String canvasId = CanvasService.instance.getCanvasId();
        if(!Utils.isEmpty(canvasId)) {
            boolean b =
                    SharePreferencesService.instance.toggleFavorite(canvasId);
            showToast(canvasId + " " +
                    getString(b ? R.string.added_to_fav : R.string.removed_from_fav));
        } else {
            showToast(R.string.no_canvas_simple);
        }
    }
    private void showFav() {
        Collection<String> ids = SharePreferencesService.instance.getAllFavoriteCanvases();
        final String[] items = ids.toArray(new String[ids.size()]);
        new AlertDialog.Builder(this)
                .setTitle(R.string.action_show_fav)
                .setItems(items,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                openCanvasOrShowToast(items[which]);
                            }
                        })
                .create().show();
    }
    private void showAbout() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.action_about)
                .setMessage(R.string.about_text)
                .create().show();
    }

    private void gotoTutorial() {
        startActivity(new Intent(this, TutorialActivity.class));
    }
    
    private void openCanvasOrShowToast(String id) {
        if(Utils.isEmpty(id)) {
            showToast(R.string.toast_empty_canvas_id);
            return;
        }
        CanvasService.instance.openCanvas(id);
    }

    @Override
    public boolean handleMessage(Message msg) {
        Log.d("MainActivity", "received message " + msg.what);
        switch (msg.what) {
            case CANVAS_CHANGE:
                String canvasId = CanvasService.instance.getCanvasId();
                showCanvasId(canvasId);
                showToast(getString(R.string.opened_canvas) + " " + canvasId);
                drawingView.reloadAllStrokes();
                return true;
            case RECEIVED_STROKE_FROM_SERVER:
                drawingView.drawStrokesFromServer();
                return true;
            case TOGGLED_FAVORITE:
                showCanvasId(CanvasService.instance.getCanvasId());
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {
            case R.id.btn_about: showAbout(); floatingActionMenu.close(true); break;
            case R.id.btn_copy_id: copyId(); floatingActionMenu.close(true); break;
            case R.id.btn_open_canvas: openCanvas(); floatingActionMenu.close(true); break;
            case R.id.btn_open_fav: showFav(); floatingActionMenu.close(true); break;
            case R.id.btn_tutorial: gotoTutorial(); floatingActionMenu.close(true); break;
            case R.id.btn_toggle_fav: toggleFav(); floatingActionMenu.close(true); break;
            default:
                Log.w("MainActivity", "View " + v.getId() + " was clicked"); break;
        }
    }

    @Override
    public void onMenuToggle(boolean b) {
        toggleTitle(b);
    }

    private void toggleTitle(boolean show) {
        Log.d("MainActivity", "toggleTitle: " + show);
        title.animate()
            .y(show ? 0 : -title.getHeight())
            .start();
    }
}

